defmodule Schoolit.Router do
  use Schoolit.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Schoolit.Auth, repo: Schoolit.Repo
  end

  #pipeline :api do
    #plug :accepts, ["json"]
  #end

  scope "/", Schoolit do
    pipe_through :browser # Use the default browser stack

    resources "/teachers", TeacherController, only: [:new, :create]
    resources "/sessions", SessionController, only: [:new, :create, :delete]
    get "/", PageController, :index
    get "/:url_code", TimeLineController, :index do
      resources "/topic", TopicController, only: [:show]
      resources "/guest_schedule", GuestScheduleController, only: [:index]
    end

    #resources "/appointments", AppointmentController, only: [] do
      #resources "/comments", CommentController, only: [:create]
    #end
  end

  #scope "/authenticated", Schoolit.Authenticated do
    #pipe_through [:browser, :authenticate_user]
  #end

  scope "/manage", Schoolit.Manage do
    pipe_through [:browser, :authenticate_teacher]
    resources "/appointments", AppointmentController
    resources "/schedules", ScheduleController, except: [:show]
    get "/profiles", ProfileController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", Schoolit do
  #   pipe_through :api
  # end
end
