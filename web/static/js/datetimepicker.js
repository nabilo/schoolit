$(document).ready(function() {
  $('.js-datetimepicker').datetimepicker({
    locale: $('html').prop('lang'),
    stepping: 15,
    format: "YYYY-MM-DD HH:mm",
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down",
      previous: "fa fa-chevron-left",
      next: "fa fa-chevron-right",
      today: "fa fa-crosshairs",
      clear: "fa fa-trash",
      close: "fa fa-remove"
    }
  });

  $('.js-timepicker').datetimepicker({
    locale: $('html').prop('lang'),
    stepping: 15,
    format: "HH:mm",
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down",
      previous: "fa fa-chevron-left",
      next: "fa fa-chevron-right",
      today: "fa fa-crosshairs",
      clear: "fa fa-trash",
      close: "fa fa-remove"
    }
  });
});

