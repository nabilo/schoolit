// https://blog.diacode.com/page-specific-javascript-in-phoenix-framework-pt-1
export default class MainView {
  mount() {
    // This will be executed when the document loads...
    //console.log('N: MainView mounted');
    //       
  }

  unmount() {
    // This will be executed when the document unloads...
    //console.log('N: MainView unmounted');
    //       
  }
}
