import MainView           from "./main";
import TimeLineIndexView from "./time_line/index";

//Collection of specific View modules
const views = {
  TimeLineIndexView
};

export default function loadView(vieName) {
  return views[vieName] || MainView;
}
