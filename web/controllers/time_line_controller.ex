defmodule Schoolit.TimeLineController do
  use Schoolit.Web, :controller
  import Schoolit.AuthGuest

  alias Schoolit.Repo

  plug :authenticate_guest

  def index(conn, params) do
    teacher = Repo.get_by(Schoolit.Teacher, url_code: params["url_code"])
    render(
      conn,
      "index.html",
      appointments: appointments(teacher),
      schedules: schedules(teacher, params["show_schedules"]),
      teacher: teacher,
      show_schedules: Map.get(params, "show_schedules", false)
    )
  end

  defp appointments(nil), do: []
  defp appointments(teacher) do
    Repo.all from a in Schoolit.Appointment,
    where: a.to > ^Ecto.DateTime.utc,
    where: a.teacher_id == ^teacher.id,
    order_by: a.from
  end

  defp schedules(teacher, "true") do
    Repo.all from schedule in Schoolit.Schedule,
      join: school_class in assoc(schedule, :school_class),
      where: school_class.teacher_id == ^teacher.id
  end
  defp schedules(_, _), do: []
end
