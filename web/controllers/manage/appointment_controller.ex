defmodule Schoolit.Manage.AppointmentController do
  use Schoolit.Web, :controller

  alias Schoolit.Appointment

  def index(conn, _params, teacher) do
    appointments = Repo.all(teacher_appointments(teacher))
    render(conn, "index.html", appointments: appointments)
  end

  def new(conn, _params, teacher) do
    changeset =
      teacher
      |> build_assoc(:appointments)
      |> Appointment.changeset()

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"appointment" => appointment_params}, teacher) do
    appointment_params = add_seconds(appointment_params)

    changeset =
      teacher
      |> build_assoc(:appointments)
      |> Appointment.changeset(appointment_params)

    case Repo.insert(changeset) do
      {:ok, _appointment} ->
        conn
        |> put_flash(:info, "Appointment created successfully.")
        |> redirect(to: appointment_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, teacher) do
    appointment = Repo.get!(teacher_appointments(teacher), id)
    render(conn, "show.html", appointment: appointment)
  end

  def edit(conn, %{"id" => id}, teacher) do
    appointment = Repo.get!(teacher_appointments(teacher), id)
    changeset = Appointment.changeset(appointment)
    render(conn, "edit.html", appointment: appointment, changeset: changeset)
  end

  def update(conn, %{"id" => id, "appointment" => appointment_params}, teacher) do
    appointment_params = add_seconds(appointment_params)
    appointment = Repo.get!(teacher_appointments(teacher), id)
    changeset = Appointment.changeset(appointment, appointment_params)

    case Repo.update(changeset) do
      {:ok, appointment} ->
        conn
        |> put_flash(:info, "Appointment updated successfully.")
        |> redirect(to: appointment_path(conn, :show, appointment))
      {:error, changeset} ->
        render(conn, "edit.html", appointment: appointment, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, teacher) do
    appointment = Repo.get!(teacher_appointments(teacher), id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(appointment)

    conn
    |> put_flash(:info, "Appointment deleted successfully.")
    |> redirect(to: appointment_path(conn, :index))
  end

  def action(conn, _) do
    apply(__MODULE__, action_name(conn), [conn, conn.params, conn.assigns.current_teacher])
  end

  defp teacher_appointments(teacher) do
    from a in Appointment,
    where: a.teacher_id == ^teacher.id,
    order_by: {:desc, a.from}
  end

  defp add_seconds(params) do
    params
    |> Map.put("from", seconds_for(params["from"]))
    |> Map.put("to", seconds_for(params["to"]))
  end

  defp seconds_for(""), do: ""
  defp seconds_for(str) do
    "#{str}:00"
  end
end
