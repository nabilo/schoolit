defmodule Schoolit.Manage.ProfileController do
	use Schoolit.Web, :controller

  def show(conn, _params, teacher) do
    render(conn, "show.html", teacher: teacher, host_url: "#{Atom.to_string(conn.scheme)}://#{conn.host}")
  end

  def action(conn, _) do
    apply(__MODULE__, action_name(conn), [conn, conn.params, conn.assigns.current_teacher])
  end
end
