defmodule Schoolit.Manage.ScheduleController do
  use Schoolit.Web, :controller

  alias Schoolit.Schedule

  def index(conn, _params, teacher) do
    schedules = Repo.all(school_class_schedules(school_class(teacher)))
    render(conn, "index.html", school_class: school_class(teacher), schedules: schedules)
  end

  def new(conn, _params, _) do
    changeset = Schedule.changeset(%Schedule{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"schedule" => schedule_params}, teacher) do
    schedule_params = add_seconds(schedule_params)
    changeset =
      school_class(teacher)
      |> build_assoc(:schedules)
      |> Schedule.changeset(schedule_params)

    case Repo.insert(changeset) do
      {:ok, _schedule} ->
        conn
        |> put_flash(:info, "Lesson Unit created successfully.")
        |> redirect(to: schedule_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}, teacher) do
    schedule = Repo.get!(school_class_schedules(school_class(teacher)), id)
    changeset = Schedule.changeset(schedule)
    render(conn, "edit.html", schedule: schedule, changeset: changeset)
  end

  def update(conn, %{"id" => id, "schedule" => schedule_params}, teacher) do
    schedule_params = add_seconds(schedule_params)
    schedule = Repo.get!(school_class_schedules(school_class(teacher)), id)
    changeset = Schedule.changeset(schedule, schedule_params)

    case Repo.update(changeset) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Schedule updated successfully.")
        |> redirect(to: schedule_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", schedule: schedule, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, teacher) do
    schedule = Repo.get!(school_class_schedules(school_class(teacher)), id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(schedule)

    conn
    |> put_flash(:info, "Schedule deleted successfully.")
    |> redirect(to: schedule_path(conn, :index))
  end

  def action(conn, _) do
    apply(__MODULE__, action_name(conn), [conn, conn.params, conn.assigns.current_teacher])
  end

  defp school_class_schedules(school_class) do
    from a in Schedule,
      where: a.school_class_id == ^school_class.id,
      order_by: [a.weekday, a.lesson_order]
  end

  defp school_class(teacher) do
    Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
  end

  defp add_seconds(params) do
    params
    |> Map.put("from", seconds_for(params["from"]))
    |> Map.put("to", seconds_for(params["to"]))
  end

  defp seconds_for(""), do: ""
  defp seconds_for(str) do
    "#{str}:00"
  end
end
