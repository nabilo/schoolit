defmodule Schoolit.PageController do
  use Schoolit.Web, :controller

  def index(conn, _params) do
    render conn, "index.html", host_url: conn.host
  end
end
