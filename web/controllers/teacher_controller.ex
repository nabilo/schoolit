defmodule Schoolit.TeacherController do
	use Schoolit.Web, :controller
  alias Schoolit.Teacher

  plug :authenticate_teacher when action in [:index, :show]

  def new(conn, _params) do
    changeset = Teacher.changeset(%Teacher{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"teacher" => teacher_params}) do
    changeset = Teacher.registration_changeset(%Teacher{}, teacher_params)
    case Repo.insert(changeset) do
      {:ok, teacher} ->
        teacher
        |> Ecto.build_assoc(:school_class, %{name: teacher.lastname, level: 1})
        |> Repo.insert!()

        conn
        |> Schoolit.Auth.login(teacher)
        |> put_flash(:info, "#{teacher.firstname} #{teacher.lastname} created!")
        |> redirect(to: appointment_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
