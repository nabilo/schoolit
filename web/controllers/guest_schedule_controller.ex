defmodule Schoolit.GuestScheduleController do
  use Schoolit.Web, :controller

  import Schoolit.AuthGuest

  alias Schoolit.Repo

  plug :authenticate_guest

  def index(conn, %{"url_code" => url_code}) do
    teacher = Repo.get_by(Schoolit.Teacher, url_code: url_code)
    schedules =
      teacher
      |> school_class()
      |> school_class_schedules()
      |> Repo.all()

    render(conn, "index.html", school_class: school_class(teacher), schedules: schedules, teacher: teacher)
  end

  defp school_class_schedules(school_class) do
    from a in Schoolit.Schedule,
      where: a.school_class_id == ^school_class.id,
      order_by: [a.weekday, a.lesson_order]
  end

  defp school_class(teacher) do
    Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
  end
end
