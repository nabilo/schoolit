defmodule Schoolit.TopicController do
  use Schoolit.Web, :controller
  import Schoolit.AuthGuest

  alias Schoolit.Appointment

  plug :authenticate_guest

  def show(conn, %{"id" => id}) do
    appointment = Repo.get!(Appointment, id)
    teacher = Repo.get!(Schoolit.Teacher, appointment.teacher_id)
    render(conn, "show.html", appointment: appointment, teacher: teacher)
  end
end
