defmodule Schoolit.SessionController do
  use Schoolit.Web, :controller

  def new(conn, _) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => %{"username" => teacher, "password" => pass}}) do
    case Schoolit.Auth.login_by_username_and_pass(conn, teacher, pass, repo: Repo) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: page_path(conn, :index))
      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Invalid username/password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Schoolit.Auth.logout()
    |> redirect(to: page_path(conn, :index))
  end
end
