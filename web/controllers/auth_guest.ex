defmodule Schoolit.AuthGuest do
  import Plug.Conn
  import Phoenix.Controller
  alias Schoolit.Router.Helpers

  def authenticate_guest(conn, _opts) do
    if find_teacher(conn.params["url_code"])do
      conn
    else
      halt_routing(conn)
    end
  end

  defp find_teacher(url_code) do
    case url_code do
      nil -> nil
      _ -> Schoolit.Repo.get_by(Schoolit.Teacher, url_code: url_code)
    end
  end

  defp halt_routing(conn) do
    conn
    |> put_flash(:error, "You can't access this page")
    |> redirect(to: Helpers.page_path(conn, :index))
    |> halt()
  end
end
