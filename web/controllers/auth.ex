defmodule Schoolit.Auth do
  import Plug.Conn
  import Phoenix.Controller
  alias Schoolit.Router.Helpers

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    teacher_id = get_session(conn, :teacher_id)

    cond do
      teacher = conn.assigns[:current_teacher] ->
        conn
      teacher = teacher_id && repo.get(Schoolit.Teacher, teacher_id) ->
        assign(conn, :current_teacher, teacher)
      true ->
        assign(conn, :current_teacher, nil)
    end
  end

  def login(conn, teacher) do
    conn
    |> assign(:current_teacher, teacher)
    |> put_session(:teacher_id, teacher.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

	import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  def login_by_username_and_pass(conn, username, given_pass, opts) do
    repo = Keyword.fetch!(opts, :repo)
    teacher = repo.get_by(Schoolit.Teacher, username: username)

    cond do
      teacher && checkpw(given_pass, teacher.password_hash) ->
        {:ok, login(conn, teacher)}
      teacher ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end

  def authenticate_teacher(conn, _opts) do
    if conn.assigns.current_teacher do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to access that page")
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end
end
