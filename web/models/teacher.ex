defmodule Schoolit.Teacher do
  use Schoolit.Web, :model

  @required_fields [:firstname, :lastname, :username, :title]
  @optional_fields []

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:username, min: 5, max: 20)
    |> unique_constraint(:username)
    |> put_url_code()
    |> unique_constraint(:url_code)
  end

  def registration_changeset(model, params) do
    model
    |> changeset(params)
    |> validate_format(:username, ~r/[\w]+@[\w]+\.[\w]+/)
    |> cast(params, ~w(password password_confirmation))
    |> validate_required([:password, :password_confirmation])
    |> validate_length(:password, min: 6, max: 100)
    |> validate_confirmation(:password)
    |> put_pass_hash()
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end

  defp put_url_code(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true} ->
        put_change(changeset, :url_code, random_string(100))
      _ ->
        changeset
    end
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end
  schema "teachers" do
    field :firstname, :string
    field :lastname,  :string
    field :username, :string
    field :title, :string
    field :url_code, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, :string
    has_many :appointments, Schoolit.Appointment, on_delete: :delete_all
    has_one :school_class, Schoolit.SchoolClass

    timestamps()
  end
end
