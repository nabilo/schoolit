defmodule Schoolit.Schedule do
  use Schoolit.Web, :model

  schema "schedules" do
    field :title, :string
    field :teacher_id, :integer
    field :content, :string
    field :weekday, :integer
    field :from, Ecto.Time
    field :to, Ecto.Time
    field :lesson_order, :integer
    belongs_to :school_class, Schoolit.SchoolClass

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do

    struct
    |> cast(params, [:title, :school_class_id, :teacher_id, :content, :weekday, :from, :to, :lesson_order])
    |> validate_required([:title, :school_class_id, :weekday, :from, :to, :lesson_order])
    |> validate_inclusion(:weekday, 1..7)
    |> validate_inclusion(:lesson_order, 1..12)
    |> validate_from_is_less_or_equal_to
    |> strip_unsafe_content(params)
    |> assoc_constraint(:school_class)
  end

  defp validate_from_is_less_or_equal_to(changeset) do
    case Ecto.Time.cast(get_field(changeset, :from)) do
      :error ->
        changeset
      _ ->
        {:ok, from} = Ecto.Time.cast(get_field(changeset, :from))
        {:ok, to} = Ecto.Time.cast(get_field(changeset, :to))

        validate_from_is_less_or_equal_to(changeset, Ecto.Time.compare(from, to))
    end
  end
  defp validate_from_is_less_or_equal_to(changeset, :gt) do
    add_error(changeset, :from, "From date cannot be after to date")
  end
  defp validate_from_is_less_or_equal_to(changeset, _), do: changeset

  defp strip_unsafe_content(model, %{"content" => nil}), do: model
  defp strip_unsafe_content(model, %{"content" => content}) do
    {:safe, clean_content} = Phoenix.HTML.html_escape(content)
    model
      |> put_change(:content, clean_content)
  end
  defp strip_unsafe_content(model, _), do: model
end
