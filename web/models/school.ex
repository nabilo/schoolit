defmodule Schoolit.School do
  use Schoolit.Web, :model

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :subdomain])
    |> validate_required([:name, :subdomain])
    |> unique_constraint(:subdomain)
  end

  schema "schools" do
    field :name, :string
    field :subdomain, :string
    field :street, :string
    field :zip_code, :string
    field :city, :string

    timestamps()
  end
end
