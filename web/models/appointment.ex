defmodule Schoolit.Appointment do
  use Schoolit.Web, :model

  @required_fields [:title, :from, :to]
  @optional_fields [:description]

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(Map.merge(params, check_to_date_time(params)), @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_from_is_less_or_equal_to
    |> strip_unsafe_description(params)
    |> assoc_constraint(:teacher)
  end

  #def topic_with_comments(id) do
    #query = from a in Appointment,
      #where: a.id == ^id,
      #preload: [:comments, :teacher]
    #Repo.one(query)
  #end

  defp check_to_date_time(%{"from" => from, "to" => to}) do
    case Ecto.DateTime.cast(to) do
      :error ->
        %{"to" => from}
      _ ->
        %{}
    end
  end
  defp check_to_date_time(%{}), do: %{}

  defp validate_from_is_less_or_equal_to(changeset) do
    case Ecto.DateTime.cast(get_field(changeset, :from)) do
      :error ->
        changeset
      _ ->
        {:ok, from} = Ecto.DateTime.cast(get_field(changeset, :from))
        {:ok, to} = Ecto.DateTime.cast(get_field(changeset, :to))

        validate_from_is_less_or_equal_to(changeset, Ecto.DateTime.compare(from, to))
    end
  end
  defp validate_from_is_less_or_equal_to(changeset, :gt) do
    add_error(changeset, :from, "From date cannot be after to date")
  end
  defp validate_from_is_less_or_equal_to(changeset, _), do: changeset

  defp strip_unsafe_description(model, %{"description" => nil}), do: model
  defp strip_unsafe_description(model, %{"description" => description}) do
    {:safe, clean_description} = Phoenix.HTML.html_escape(description)
    model
      |> put_change(:description, clean_description)
  end
  defp strip_unsafe_description(model, _), do: model

  schema "appointments" do
    field :title, :string
    field :description, :string
    field :from, Ecto.DateTime
    field :to, Ecto.DateTime
    belongs_to :teacher, Schoolit.Teacher
    belongs_to :school_class, Schoolit.SchoolClass

    timestamps()
  end
end
