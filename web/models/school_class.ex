defmodule Schoolit.SchoolClass do
  use Schoolit.Web, :model

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:level, :group, :name, :description])
    |> validate_required([:name])
    |> assoc_constraint(:teacher)
    |> assoc_constraint(:school)
  end

  schema "school_classes" do
    field :level, :integer
    field :group, :string
    field :name, :string
    field :description, :string
    belongs_to :teacher, Schoolit.Teacher
    belongs_to :school, Schoolit.School
    has_many :schedules, Schoolit.Schedule

    timestamps()
  end
end
