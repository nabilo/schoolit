defmodule Schoolit.TimeLineView do
  use Schoolit.Web, :view

  defp time_line_items(attributes) do
    %{id: nil, title: nil, content: nil, weekday: nil, from: nil, to: nil, type: nil}
    |> Map.merge(attributes)
  end

  def appointments_and_schedules(appointments, schedules) do
    grouped_schedules = grouped_schedules_by_weekday(schedules)

    line_time_items = grouped_appointments_by_date(appointments)
    |> add_schedules_to_appointments(grouped_schedules)

    first_week_schedules_without_appointments = first_week(line_time_items, grouped_schedules)

    Map.merge(line_time_items, first_week_schedules_without_appointments)
  end

  defp add_schedules_to_appointments(grouped_appointments, grouped_schedules) do
    grouped_appointments
    |> Enum.reduce(%{}, fn({date, appointments}, acc) ->
      weekday = date |> Ecto.Date.to_erl |> Date.from_erl! |> Date.day_of_week
      Map.merge(acc, add_schedule_to_the_day(date, Map.get(grouped_schedules, weekday, []), appointments))
    end)
  end

  def grouped_appointments_by_date(appointments) do
    appointments
    |> Enum.group_by(&(Ecto.DateTime.to_date(&1.from)))
    |> Enum.reduce(%{}, fn({date, day_appointments}, acc) ->
      day_appointments = Enum.map day_appointments, fn(appointment) ->
        appointment |> prepare_struct("appointment") |> time_line_items()
      end
      Map.put(acc, date, day_appointments)
    end)
  end

  def grouped_schedules_by_weekday(schedules) do
    schedules
    |> Enum.group_by(&(&1.weekday))
    |> Enum.reduce(%{}, fn({weekday, day_schedules}, acc) ->
      day_schedules =  Enum.map day_schedules, fn(schedule) ->
        schedule |> prepare_struct("schedule") |> time_line_items()
      end
      Map.put(acc, weekday, day_schedules)
    end)
  end

  defp prepare_struct(item, type) do
    if type == "appointment" do
      %{
        id: item.id,
        type: type,
        title: item.title,
        content: item.description,
        from: Ecto.DateTime.to_time(item.from),
        to: Ecto.DateTime.to_time(item.to),
        weekday: item.from |> Ecto.DateTime.to_date |> Ecto.Date.to_erl |> Date.from_erl! |> Date.day_of_week
      }
    else
      %{
        id: item.id,
        type: type,
        title: item.title,
        content: item.content,
        from: item.from,
        to: item.to,
        weekday: item.weekday
      }
    end
  end

  defp first_week(grouped_appointments_and_chedules, grouped_schedules) do
    Enum.at(grouped_appointments_and_chedules, 0, nil)
    |> first_date
    |> seven_days_from_date
    |> Enum.reduce(%{}, fn(date, acc) ->
      weekday = date |> Ecto.Date.to_erl |> Date.from_erl! |> Date.day_of_week

      Map.merge(acc, add_schedule_to_first_week_days_without_appointments(
        date, Map.get(grouped_schedules, weekday, []), Map.get(grouped_appointments_and_chedules, date, [])
      ))
    end)
  end
  defp add_schedule_to_first_week_days_without_appointments(_, [], []), do: %{}
  defp add_schedule_to_first_week_days_without_appointments(date, schedules, []), do: %{date => schedules}
  defp add_schedule_to_first_week_days_without_appointments(date, _, time_line_items), do: %{date => time_line_items}

  def add_schedule_to_the_day(date, [], appointments), do: %{date => appointments}
  def add_schedule_to_the_day(date, schedules, appointments) do
    sorted = schedules ++ appointments
             |> Enum.sort_by(fn(el) -> el.from |> Ecto.Time.to_erl end)

    %{date => sorted}
  end

  defp seven_days_from_date(date) do
    (0..6)
    |> Enum.reduce([], fn(days, acc) -> [date_after_days(date, days)|acc] end)
    |> Enum.reverse
  end

  defp date_after_days(ecto_date, days) do
    ecto_date
    |> Ecto.Date.to_erl
    |> :calendar.date_to_gregorian_days
    |> Kernel.+(days)
    |> :calendar.gregorian_days_to_date
    |> Ecto.Date.from_erl
  end

  defp first_date({date, _}), do: date
  defp first_date(nil), do: Date.utc_today |> Date.to_erl |> Ecto.Date.from_erl

  def first_element_id([head|_]), do: head.id

  def toggle_show_schedules("true"), do: false
  def toggle_show_schedules(_), do: true

  def show_schedule_link_text("true"), do: "Hide Timetable"
  def show_schedule_link_text(_), do: "Show Timetable"
end
