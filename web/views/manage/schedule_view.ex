defmodule Schoolit.Manage.ScheduleView do
  use Schoolit.Web, :view

  def weekdays do
    %{1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday", 7 => "Sunday"}
  end

  def grouped_by_weekday(schedules) do
    Enum.group_by(schedules, fn(schedule) -> schedule.weekday end)
  end

  def grouped_by_lesson_order(schedules) do
    Enum.group_by(schedules, fn(schedule) -> schedule.lesson_order end)
  end

  def cols_per_day(grouped_schedules) when map_size(grouped_schedules) > 0 do
    Float.to_string(10/Enum.count(grouped_schedules))
    |> String.split(".")
    |> List.first
    |> String.to_integer
  end
  def cols_per_day(_), do: 1

  def max_lessons_per_week(schedules) when length(schedules) > 0 do
    Enum.map(schedules, fn(schedule) -> schedule.lesson_order end)
    |> Enum.max
  end
  def max_lessons_per_week(_), do: 1

  def remove_seconds(time) do
    Ecto.Time.to_string(time)
    |> String.slice(0..4)
  end

  #require IEx
  #def puk(schedule) do
    #IEx.pry
  #end
end
