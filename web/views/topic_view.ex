defmodule Schoolit.TopicView do
  use Schoolit.Web, :view

  def markdown(text) do
    text
    |> Earmark.to_html
    |> raw
  end
end
