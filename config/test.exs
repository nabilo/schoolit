use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :schoolit, Schoolit.Endpoint,
  http: [port: 4001],
  server: false

config :comeonin, :bcrypt_log_rounds, 4

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :schoolit, Schoolit.Repo,
  adapter: Ecto.Adapters.Postgres,
  #username: "postgres",
  #password: "postgres",
  database: "schoolit_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
