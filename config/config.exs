# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :schoolit,
  ecto_repos: [Schoolit.Repo]

# Configures the endpoint
config :schoolit, Schoolit.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "D/9GDFOTB6HL5SCkMH6YUikiZZhA9coKUxvdm3sV63AMZ4N8E2hjrjYj3Ycpu7xh",
  render_errors: [view: Schoolit.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Schoolit.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
