defmodule Schoolit.CommentController do
  use Schoolit.Web, :controller

  alias Schoolit.Comment

  def create(conn, %{"comment" => comment_params}) do
    appointment = Repo.get!(Schoolit.Appointment, comment_params[:appointment_id])

    changeset =
      appointment
      |> build_assoc(:comments)
      |> Comment.changeset(comment_params)

    case Repo.insert(changeset) do
      {:ok, _comment} ->
        changeset = appointment |> build_assoc(:comments)
        conn
        |> put_flash(:info, "Comment created successfully.")
        |> redirect(to: topic_path(conn, :show, appointment))
      {:error, changeset} ->
        render(conn, "topic/show.html", changeset: changeset, appointment: appointment)
    end
  end
end
