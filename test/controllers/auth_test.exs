defmodule Schoolit.AuthTest do
  use Schoolit.ConnCase
  alias Schoolit.Auth

  setup %{conn: conn} do
    conn =
      conn
      |> bypass_through(Schoolit.Router, :browser)
      |> get("/")

    {:ok, %{conn: conn}}
  end

  test "authenticate_teacher halts when no current_teacher exists", %{conn: conn} do
    conn =
      conn
      |> assign(:current_teacher, nil)
      |> Auth.authenticate_teacher([])

    assert conn.halted
  end

  test "authenticate_teacher continues when the current_teacher exists", %{conn: conn} do
    conn =
      conn
      |> assign(:current_teacher, %Schoolit.Teacher{})
      |> Auth.authenticate_teacher([])

      refute conn.halted
  end

  test "login puts the user in the session", %{conn: conn} do
    login_conn =
      conn
      |> Auth.login(%Schoolit.Teacher{id: 123})
      |> send_resp(:ok, "")

    next_conn = get(login_conn, "/")
    assert get_session(next_conn, :teacher_id) == 123
  end

  test "logout drops the session", %{conn: conn} do
    logout_conn =
      conn
      |> put_session(:user_id, 123)
      |> Auth.logout()
      |> send_resp(:ok, "")

    next_conn = get(logout_conn, "/")
    refute get_session(next_conn, :user_id)
  end

  test "call places teacher from session into assigns", %{conn: conn} do
    teacher = insert_teacher(%{username: "ma@mi.de"})
    conn =
      conn
      |> put_session(:teacher_id, teacher.id)
      |> Auth.call(Repo)

      assert conn.assigns.current_teacher.id == teacher.id
  end

  test "call with no session sets current_teacher assign to nil", %{conn: conn} do
    conn = Auth.call(conn, Repo)
    assert conn.assigns.current_teacher == nil
  end

  test "login with valid username and pass", %{conn: conn} do
    teacher = insert_teacher(%{username: "me_su@xx.de", password: "secret", password_confirmation: "secret"})
    {:ok, conn} = Auth.login_by_username_and_pass(conn, "me_su@xx.de", "secret", repo: Repo)

    assert conn.assigns.current_teacher.id == teacher.id
  end

  test "login with a not found user", %{conn: conn} do
    assert({:error, :not_found, _conn} =
      Auth.login_by_username_and_pass(conn, "xyz_teach@xx.de", "secret", repo: Repo))
  end

  test "login with password mismatch", %{conn: conn} do
    insert_teacher(%{username: "me_su@xx.de", password: "secret", password_confirmation: "secret"})

    assert({:error, :unauthorized, _conn} =
      Auth.login_by_username_and_pass(conn, "me_su@xx.de", "wrong", repo: Repo))
  end
end
