defmodule Schoolit.Manage.ScheduleControllerTest do
  use Schoolit.ConnCase

  alias Schoolit.Schedule

  @valid_attrs %{
    weekday: 2,
    lesson_order: 1,
    from: "14:00",
    to: "15:00",
    title: "some content"
  }
  @invalid_attrs %{description: "", from: nil, to: nil, title: ""}
  defp schedule_count(query), do: Repo.one(from v in query, select: count(v.id))

  setup %{conn: conn} = config do
    if username = config[:login_as] do
      teacher = insert_teacher(%{username: username})
      insert_school_class(teacher)
      conn = assign(build_conn(), :current_teacher, teacher)
      {:ok, conn: conn, teacher: teacher}
    else
      :ok
    end
  end

  @tag login_as: "maximus@minimus.xus"
  test "lists all school_class schedules on index", %{conn: conn, teacher: teacher} do
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
    schedule1 = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))
    schedule2 = insert_schedule(
     insert_school_class(insert_teacher(%{username: "xosfos@xx.de"})),
     Map.merge(@valid_attrs, %{title: "super date with it"})
   )
    conn = get conn, schedule_path(conn, :index)

    assert html_response(conn, 200) =~ "Listing the timetable for class #{school_class.name}"
    assert String.contains?(conn.resp_body, schedule1.title)
    refute String.contains?(conn.resp_body, schedule2.title)
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders form for new resources", %{conn: conn} do
    conn = get conn, schedule_path(conn, :new)

    assert html_response(conn, 200) =~ "New Timetable Lesson Unit"
  end

  @tag login_as: "maximus@minimus.xus"
  test "creates resource and redirects when data is valid", %{conn: conn} do
    count_before = schedule_count(Schedule)
    conn = post conn, schedule_path(conn, :create), schedule: @valid_attrs

    assert redirected_to(conn) == schedule_path(conn, :index)
    assert schedule_count(Schedule) == count_before + 1
  end

  @tag login_as: "maximus@minimus.xus"
  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    count_before = schedule_count(Schedule)
    conn = post conn, schedule_path(conn, :create), schedule: @invalid_attrs
    assert html_response(conn, 200) =~ "New Timetable Lesson Unit"
    assert schedule_count(Schedule) == count_before
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, schedule_path(conn, :edit, -1)
    end
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders form for editing chosen resource", %{conn: conn, teacher: teacher} do
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
    schedule = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))
    conn = get conn, schedule_path(conn, :edit, schedule)

    assert html_response(conn, 200) =~ "Edit School Timetable Lesson"
  end

  @tag login_as: "maximus@minimus.xus"
  test "updates chosen resource and redirects when data is valid", %{conn: conn, teacher: teacher} do
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
    schedule = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))
    conn = put conn, schedule_path(conn, :update, schedule), schedule: @valid_attrs

    assert html_response(conn, 302)
    assert redirected_to(conn) == schedule_path(conn, :index)
  end

  @tag login_as: "maximus@minimus.xus"
  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn, teacher: teacher} do
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
    schedule = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))
    conn = put conn, schedule_path(conn, :update, schedule), schedule: @invalid_attrs

    assert html_response(conn, 200) =~ "Edit School Timetable Lesson"
  end

  @tag login_as: "maximus@minimus.xus"
  test "deletes chosen resource", %{conn: conn, teacher: teacher} do
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^teacher.id)
    schedule = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))
    conn = delete conn, schedule_path(conn, :delete, schedule)

    assert redirected_to(conn) == schedule_path(conn, :index)
    refute Repo.get(Schedule, schedule.id)
  end

  test "requires user authentication on all actions", %{conn: conn} do
    Enum.each(
    [
      get(conn, schedule_path(conn, :new)),
      get(conn, schedule_path(conn, :index)),
      get(conn, schedule_path(conn, :edit, "123")),
      put(conn, schedule_path(conn, :update, "123", %{})),
      post(conn, schedule_path(conn, :create, %{})),
      delete(conn, schedule_path(conn, :delete, "123")),
    ], fn conn ->
         assert html_response(conn, 302)
         assert conn.halted
       end
    )
  end

  @tag login_as: "maximus@minimus.xus"
  test "authorizes actions against access by other teacher", %{conn: conn} do
    owner = insert_teacher(%{username: "sneaky@xx.de"})
    insert_school_class(owner)
    school_class = Repo.one(from a in Schoolit.SchoolClass, where: a.teacher_id == ^owner.id)
    schedule = insert_schedule(school_class, Map.merge(@valid_attrs, %{title: "show me on date"}))

    assert_error_sent :not_found, fn ->
      get(conn, schedule_path(conn, :edit, schedule))
    end
    assert_error_sent :not_found, fn ->
      put(conn, schedule_path(conn, :update, schedule, schedule: @valid_attrs))
    end
    assert_error_sent :not_found, fn ->
      delete(conn, schedule_path(conn, :delete, schedule))
    end
  end
end
