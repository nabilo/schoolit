defmodule Schoolit.Manage.AppointmentControllerTest do
  use Schoolit.ConnCase

  alias Schoolit.Appointment
  @valid_attrs %{description: "some content",
    from: "2010-05-19 14:00",
    to: "2010-05-19 14:00",
    title: "some content"}
  @invalid_attrs %{description: "", from: nil, to: nil, title: ""}
  defp appointment_count(query), do: Repo.one(from v in query, select: count(v.id))

  setup %{conn: conn} = config do
    if username = config[:login_as] do
      teacher = insert_teacher(%{username: username})
      conn = assign(build_conn(), :current_teacher, teacher)
      {:ok, conn: conn, teacher: teacher}
    else
      :ok
    end
  end

  @tag login_as: "maximus@minimus.xus"
  test "lists all teacher appointments on index", %{conn: conn, teacher: teacher} do
    appointment1 = insert_appoint(teacher, Map.merge(@valid_attrs, %{title: "show me on date"}))
    appointment2 = insert_appoint(
     insert_teacher(%{username: "xosfos@xx.de"}), Map.merge(@valid_attrs, %{title: "super date wit it"})
   )

    conn = get conn, appointment_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing appointments"
    assert String.contains?(conn.resp_body, appointment1.title)
    refute String.contains?(conn.resp_body, appointment2.title)
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders form for new resources", %{conn: conn} do
    conn = get conn, appointment_path(conn, :new)
    assert html_response(conn, 200) =~ "New appointment"
  end

  @tag login_as: "maximus@minimus.xus"
  test "creates resource and redirects when data is valid", %{conn: conn} do
    count_before = appointment_count(Appointment)
    conn = post conn, appointment_path(conn, :create), appointment: @valid_attrs
    assert redirected_to(conn) == appointment_path(conn, :index)
    assert appointment_count(Appointment) == count_before + 1
  end

  @tag login_as: "maximus@minimus.xus"
  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    count_before = appointment_count(Appointment)
    conn = post conn, appointment_path(conn, :create), appointment: @invalid_attrs
    assert html_response(conn, 200) =~ "New appointment"
    assert appointment_count(Appointment) == count_before
  end

  @tag login_as: "maximus@minimus.xus"
  test "shows chosen resource", %{conn: conn, teacher: teacher} do
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = get conn, appointment_path(conn, :show, appointment)
    assert html_response(conn, 200) =~ "Show appointment"
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, appointment_path(conn, :show, -1)
    end
  end

  @tag login_as: "maximus@minimus.xus"
  test "renders form for editing chosen resource", %{conn: conn, teacher: teacher} do
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = get conn, appointment_path(conn, :edit, appointment)
    assert html_response(conn, 200) =~ "Edit appointment"
  end

  @tag login_as: "maximus@minimus.xus"
  test "updates chosen resource and redirects when data is valid", %{conn: conn, teacher: teacher} do
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = put conn, appointment_path(conn, :update, appointment), appointment: @valid_attrs
    assert redirected_to(conn) == appointment_path(conn, :show, appointment)
  end

  @tag login_as: "maximus@minimus.xus"
  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn, teacher: teacher} do
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = put conn, appointment_path(conn, :update, appointment), appointment: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit appointment"
  end

  @tag login_as: "maximus@minimus.xus"
  test "deletes chosen resource", %{conn: conn, teacher: teacher} do
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = delete conn, appointment_path(conn, :delete, appointment)
    assert redirected_to(conn) == appointment_path(conn, :index)
    refute Repo.get(Appointment, appointment.id)
  end

  test "requires user authentication on all actions", %{conn: conn} do
    Enum.each(
    [
      get(conn, appointment_path(conn, :new)),
      get(conn, appointment_path(conn, :index)),
      get(conn, appointment_path(conn, :show, "123")),
      get(conn, appointment_path(conn, :edit, "123")),
      put(conn, appointment_path(conn, :update, "123", %{})),
      post(conn, appointment_path(conn, :create, %{})),
      delete(conn, appointment_path(conn, :delete, "123")),
    ], fn conn ->
         assert html_response(conn, 302)
         assert conn.halted
       end
    )
  end

  @tag login_as: "maximus@minimus.xus"
  test "authorizes actions against access by other teacher", %{teacher: owner, conn: conn} do
    appointment = insert_appoint(owner, @valid_attrs)
    non_owner = insert_teacher(%{username: "sneaky@xx.de"})
    conn = assign(conn, :current_teacher, non_owner)

    assert_error_sent :not_found, fn ->
      get(conn, appointment_path(conn, :show, appointment))
    end
    assert_error_sent :not_found, fn ->
      get(conn, appointment_path(conn, :edit, appointment))
    end
    assert_error_sent :not_found, fn ->
      put(conn, appointment_path(conn, :update, appointment, appointment: @valid_attrs))
    end
    assert_error_sent :not_found, fn ->
      delete(conn, appointment_path(conn, :delete, appointment))
    end
  end
end
