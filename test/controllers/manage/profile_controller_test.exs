defmodule Schoolit.Manage.ProfileControllerTest do
  use Schoolit.ConnCase

  setup %{conn: conn} = config do
    if username = config[:login_as] do
      teacher = insert_teacher(%{username: username})
      conn = assign(build_conn(), :current_teacher, teacher)
      {:ok, conn: conn, teacher: teacher}
    else
      :ok
    end
  end

  @tag login_as: "maximus@minimus.xus"
  test "shows teacher username on show", %{conn: conn, teacher: teacher} do
    conn = get conn, profile_path(conn, :show)
    assert html_response(conn, 200) =~ "Showing Teacher"
    assert String.contains?(conn.resp_body, teacher.username)
  end
end
