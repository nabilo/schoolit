defmodule Schoolit.AuthGuestTest do
  use Schoolit.ConnCase
  alias Schoolit.AuthGuest

  test "allow if url_code is in params and teacher exists" do
    teacher = insert_teacher(%{username: "maximus@minimux.de"})
    conn =
      build_conn()
      |> bypass_through(Schoolit.Router, :browser)
      |> get("/", url_code: teacher.url_code)
      |> AuthGuest.authenticate_guest([])

      refute conn.halted
  end

  test "halt connection if url_code doesn't exist in the database" do
    conn =
      build_conn()
      |> bypass_through(Schoolit.Router, :browser)
      |> get("/", url_code: "xxxx")
      |> AuthGuest.authenticate_guest([])

      assert conn.halted
  end

  test "halt connection if url_code is not in params" do
    conn =
      build_conn()
      |> bypass_through(Schoolit.Router, :browser)
      |> get("/")
      |> AuthGuest.authenticate_guest([])

    assert conn.halted
  end
end
