defmodule Schoolit.TimeLineControllerTest do
  use Schoolit.ConnCase

  test "lists all teacher appointments on index", %{conn: conn} do
    future_appointment = %{description: "some content",
      from: ten_oclock_future(1),
      to: ten_oclock_future(1),
      title: "some content"}

    past_appointment = %{
      from: ten_oclock_past(1),
      to: ten_oclock_past(1),
      title: "other content"}
    teacher = insert_teacher(%{username: "maximus@minimus.xus"})
    school_class = insert_school_class(teacher)

    appointment1 = insert_appoint(teacher, Map.merge(future_appointment, %{title: "show me 1.a"}))
    appointment2 = insert_appoint(teacher, Map.merge(future_appointment, %{title: "show me 1.b"}))
    from_to_3 = ten_oclock_future(2)
    appointment3 = insert_appoint(
      teacher, Map.merge(future_appointment, %{from: from_to_3, to: from_to_3, title: "show me 2"})
    )

    appointment4 = insert_appoint(
     teacher, Map.merge(past_appointment, %{title: "Hide it 1"})
    )
    appointment5 = insert_appoint(
     insert_teacher(%{username: "xosfos@xx.de"}), Map.merge(future_appointment, %{title: "show it 3"})
    )

    schedule = insert_schedule(school_class, %{title: "Schedule Title is shown with schedules"})

    conn = get conn, time_line_path(conn, :index, teacher.url_code, show_schedules: false)
    assert html_response(conn, 200) =~ "What is next"

    assert String.contains?(conn.resp_body, appointment1.title)
    assert String.contains?(conn.resp_body, appointment2.title)
    date_1_2 = Ecto.DateTime.to_date(appointment1.from) |> Ecto.Date.to_string
    assert String.contains?(conn.resp_body, date_1_2)

    assert String.contains?(conn.resp_body, appointment3.title)
    date_3 = Ecto.DateTime.to_date(appointment3.from) |> Ecto.Date.to_string
    assert String.contains?(conn.resp_body, date_3)

    refute String.contains?(conn.resp_body, appointment4.title)
    date_4 = Ecto.DateTime.to_date(appointment4.from) |> Ecto.Date.to_string
    refute String.contains?(conn.resp_body, date_4)

    refute String.contains?(conn.resp_body, appointment5.title)
    refute String.contains?(conn.resp_body, schedule.title)
  end

  test "lists teacher appointments and class schedules", %{conn: conn} do
    future_appointment = %{description: "some content",
      from: ten_oclock_future(1),
      to: ten_oclock_future(1),
      title: "some content"}

    teacher = insert_teacher(%{username: "maximus@minimus.xus"})
    school_class = insert_school_class(teacher)
    appointment1 = insert_appoint(teacher, Map.merge(future_appointment, %{title: "show me 1.a"}))
    appointment2 = insert_appoint(teacher, Map.merge(future_appointment, %{title: "show me 1.b"}))
    from_to_3 = ten_oclock_future(2)
    appointment3 = insert_appoint(
      teacher, Map.merge(future_appointment, %{from: from_to_3, to: from_to_3, title: "show me 2"})
    )

    appointment5 = insert_appoint(
     insert_teacher(%{username: "xosfos@xx.de"}), Map.merge(future_appointment, %{title: "show it 3"})
    )
    schedule = insert_schedule(school_class, %{title: "Schedule Title is shown with schedules"})

    conn = get conn, time_line_path(conn, :index, teacher.url_code, show_schedules: true)
    assert html_response(conn, 200) =~ "What is next"

    assert String.contains?(conn.resp_body, appointment1.title)
    assert String.contains?(conn.resp_body, appointment2.title)
    date_1_2 = Ecto.DateTime.to_date(appointment1.from) |> Ecto.Date.to_string
    assert String.contains?(conn.resp_body, date_1_2)

    assert String.contains?(conn.resp_body, appointment3.title)
    date_3 = Ecto.DateTime.to_date(appointment3.from) |> Ecto.Date.to_string
    assert String.contains?(conn.resp_body, date_3)

    assert String.contains?(conn.resp_body, schedule.title)

    refute String.contains?(conn.resp_body, appointment5.title)
  end
end
