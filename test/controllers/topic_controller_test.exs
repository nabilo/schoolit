defmodule Schoolit.TopicControllerTest do
  use Schoolit.ConnCase

  @valid_attrs %{description: "some content!!!",
    from: "2010-05-19 14:00",
    to: "2010-05-20 14:00",
    title: "some Title???"}

  test "shows chosen resource", %{conn: conn} do
    teacher = insert_teacher(%{username: "maximus@minimus.xus"})
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = get conn, topic_path(conn, :show, appointment, url_code: teacher.url_code)
    assert html_response(conn, 200) =~ @valid_attrs[:title]
    assert String.contains?(conn.resp_body, @valid_attrs[:description])
  end

  test "redirects to page_path if url_code is wrong", %{conn: conn} do
    teacher = insert_teacher(%{username: "maximus@minimus.xus"})
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = get conn, topic_path(conn, :show, appointment, url_code: "wrong url code")
    assert redirected_to(conn) == page_path(conn, :index)
  end

  test "redirects to page_path if url_code is missing", %{conn: conn} do
    teacher = insert_teacher(%{username: "maximus@minimus.xus"})
    appointment = insert_appoint(teacher, @valid_attrs)
    conn = get conn, topic_path(conn, :show, appointment)
    assert redirected_to(conn) == page_path(conn, :index)
  end
end
