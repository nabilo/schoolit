defmodule Schoolit.PageControllerTest do
  use Schoolit.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to What next? app"
  end
end
