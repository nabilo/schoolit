defmodule Schoolit.TeacherRepoTest do
  use Schoolit.ModelCase
  alias Schoolit.Teacher

  @valid_attrs %{title: "f", firstname: "f_teacher", lastname: "xx", username: "maximus@minimus.xus"}

  test "converts unique_constraint on username to error" do
    insert_teacher(%{username: "ericson@xx.de"})
    attrs = Map.put(@valid_attrs, :username, "ericson@xx.de")
    changeset = Teacher.changeset(%Teacher{}, attrs)

    assert {:error, changeset} = Repo.insert(changeset)
    assert {:username, {"has already been taken", []}} in changeset.errors
  end
end
