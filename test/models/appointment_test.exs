defmodule Schoolit.AppointmentTest do
  use Schoolit.ModelCase

  alias Schoolit.Appointment

  @valid_attrs %{
    "description" => "some content",
    "from" => %{"day" => 17, "hour" => 14, "min" => 0, "month" => 4, "sec" => 0, "year" => 2010},
    "title" => "some content",
    "to" => %{"day" => 18, "hour" => 14, "min" => 0, "month" => 4, "sec" => 0, "year" => 2010}
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Appointment.changeset(%Appointment{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Appointment.changeset(%Appointment{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "set to_datetime equal from_datetime if it doesn't exist" do
    changeset = Appointment.changeset(%Appointment{}, Map.put(@valid_attrs, "to", nil))

    assert changeset.valid?
    assert get_change(changeset, :to) == get_change(changeset, :from)
  end

  test "vaidate from_datetime date is less or equal to_datetime" do
    changeset = Appointment.changeset(
      %Appointment{}, Map.put(@valid_attrs["from"], "year", @valid_attrs["from"]["year"] + 1)
    )

    refute changeset.valid?
  end

  # (.topic_with_comments/1)
  #test "topic_with_comments return appointment to the id where comments and teacher are preloaded" do
    #teacher = insert_teacher
    #appointment = insert_appoint(teacher)
    #insert_comment(appointment)
    #insert_comment(appointment)
    #comment3 = insert_comment(nil)

    #returned_appointment = Appointment.topic_with_comments(appointment.id)
    #assert returned_appointment.id == appointment.id
    #assert returned_appointment.teacher.id == teacher.id
    #assert length(returned_appointment.comments) == 2
    #refute Enum.member?(returned_appointment.comments, comment3)
  #end


  test "when the description includes a script tag" do
    changeset = Appointment.changeset(%Appointment{}, %{@valid_attrs | "description" => "Hello <script type='javascript'>alert('foo');</script>"})
    refute String.match?(get_change(changeset, :description), ~r{<script>})
  end

  test "when the description includes an iframe tag" do
    changeset = Appointment.changeset(%Appointment{}, %{@valid_attrs | "description" => "Hello <iframe src='http://google.com'></iframe>"})
    refute String.match?(get_change(changeset, :description), ~r{<iframe>})
  end

  test "description includes no stripped tags" do
    changeset = Appointment.changeset(%Appointment{}, @valid_attrs)
    assert get_change(changeset, :description) == @valid_attrs["description"]
  end
end
