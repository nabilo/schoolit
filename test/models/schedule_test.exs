defmodule Schoolit.ScheduleTest do
  use Schoolit.ModelCase

  alias Schoolit.Schedule

  @valid_attrs %{content: "some content", school_class_id: 42, teacher_id: 42, from: %{hour: 14, min: 0, sec: 0}, to: %{hour: 14, min: 0, sec: 0}, title: "some content", weekday: 7, lesson_order: 2}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Schedule.changeset(%Schedule{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with empty attributes is invalid" do
    changeset = Schedule.changeset(%Schedule{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "vaidate from_time date is less or equal to_time" do
    changeset = Schedule.changeset(
      %Schedule{}, Map.put(@valid_attrs[:from], :min, @valid_attrs[:from][:min] + 1)
    )

    refute changeset.valid?
  end

  test "when the content includes a script tag" do
    changeset = Schedule.changeset(%Schedule{}, %{@valid_attrs | :content => "Hello <script type='javascript'>alert('foo');</script>"})
    refute String.match?(get_change(changeset, :content), ~r{<script>})
  end

  test "when the content includes an iframe tag" do
    changeset = Schedule.changeset(%Schedule{}, %{@valid_attrs | :content => "Hello <iframe src='http://google.com'></iframe>"})
    refute String.match?(get_change(changeset, :content), ~r{<iframe>})
  end

  test "content includes no stripped tags" do
    changeset = Schedule.changeset(%Schedule{}, @valid_attrs)
    assert get_change(changeset, :content) == @valid_attrs[:content]
  end
end
