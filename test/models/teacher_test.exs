defmodule Schoolit.TeacherTest do
  use Schoolit.ModelCase, async: true
  alias Schoolit.Teacher

  @valid_attrs %{title: "m", firstname: "f_teacher", lastname: "l_teacher", username: "maximus@minimus.xus", password: "secret"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Teacher.changeset(%Teacher{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Teacher.changeset(%Teacher{}, @invalid_attrs)

    refute changeset.valid?
  end

  test "changeset does not accept long usernames" do
    attrs = Map.put(@valid_attrs, :username, String.duplicate("a", 30))

    assert {:username, "should be at most 20 character(s)"} in errors_on(%Teacher{}, attrs)
  end

  test "registration_changeset password must be at least 6 chars long" do
    attrs = Map.put(@valid_attrs, :password, "12345")
    attrs = Map.put(attrs, :password_confirmation, "12345")
    changeset = Teacher.registration_changeset(%Teacher{}, attrs)

    refute changeset.valid?
    assert  {
      :password, {"should be at least %{count} character(s)", [count: 6, validation: :length, min: 6]}
    } in changeset.errors
  end

  test "registration_changeset with valid attributes hashes password" do
    attrs = Map.put(@valid_attrs, :password, "123456")
    attrs = Map.put(attrs, :password_confirmation, "123456")
    changeset = Teacher.registration_changeset(%Teacher{}, attrs)
    %{password: pass, password_hash: pass_hash} = changeset.changes

    assert changeset.valid?
    assert pass_hash
    assert Comeonin.Bcrypt.checkpw(pass, pass_hash)
  end
end
