defmodule Schoolit.TimeLineViewTest do
  use Schoolit.ConnCase, async: true

  test "#appointments_and_schedules: return empty string if no appointments and no schedules exists" do
    assert %{} == Schoolit.TimeLineView.appointments_and_schedules([], [])
  end

  test "#appointments_and_schedules: returns time_line_items of appointments" do
    app1 = fill_appointment(6)
    app2 = fill_appointment(4)
    app3 = fill_appointment(1)
    app4 = fill_appointment(4)
    date1 = after_days(6) |> Ecto.DateTime.to_date
    date2 = after_days(4) |> Ecto.DateTime.to_date
    date3 = after_days(1) |> Ecto.DateTime.to_date

    appointments = [app1, app2, app3, app4]

    expected_result = %{
      date3 => [apppointment_to_time_line_items(app3)],
      date2 => [apppointment_to_time_line_items(app2), apppointment_to_time_line_items(app4)],
      date1 => [apppointment_to_time_line_items(app1)],
    }

    assert expected_result == Schoolit.TimeLineView.appointments_and_schedules(appointments, [])
  end

  test "#appointments_and_schedules: returns time_line_items of appointments and schedules sorted by time" do
    sch1 = fill_schedule(1)
    sch2 = fill_schedule(-1)
    app1 = fill_appointment(0)
    app2 = fill_appointment(0)
    app3 = fill_appointment(1)
    date1 = after_days(0) |> Ecto.DateTime.to_date
    date2 = after_days(1) |> Ecto.DateTime.to_date

    appointments = [app1, app2, app3]
    schedules = [sch2, sch1]

    expected_result = %{
      date1 => [
        schedule_to_time_line_items(sch2),
        apppointment_to_time_line_items(app1),
        apppointment_to_time_line_items(app2),
        schedule_to_time_line_items(sch1)
      ],
      date2 => [apppointment_to_time_line_items(app3)],
    }

    assert expected_result == Schoolit.TimeLineView.appointments_and_schedules(appointments,schedules)
  end

  test "returns first element id" do
    my_list = [%{id: 44}, %{id: 22}]
    assert 44 == Schoolit.TimeLineView.first_element_id(my_list)
  end

  def apppointment_to_time_line_items(app) do
    %{id: app.id, title: app.title, content: "",
      weekday: app.from |> Ecto.DateTime.to_date |> Ecto.Date.to_erl |> Date.from_erl! |> Date.day_of_week,
      from: Ecto.DateTime.to_time(app.from), to: Ecto.DateTime.to_time(app.to), type: "appointment"}
  end

  def schedule_to_time_line_items(sched) do
    %{id: sched.id, title: sched.title, content: "", weekday: sched.weekday,
      from: sched.from, to: sched.to, type: "schedule"}
  end

  def fill_appointment(number) do
    datetime = after_days(number)
    %{
      id: 1,
      title: "title #{number}",
      description: "",
      from: datetime,
      to: datetime
    }
  end

  def fill_schedule(number) do
    tmp = Ecto.DateTime.utc |> Ecto.DateTime.to_time
    hour = tmp.hour + number
    {:ok, time} = Ecto.Time.cast({hour,0,0})
    %{
      id: 1,
      title: "title #{number}",
      content: "",
      from: time,
      to: time,
      weekday: Ecto.Date.utc |> Ecto.Date.to_erl |> Date.from_erl! |> Date.day_of_week
    }
  end

  def after_days(days) do
    {{a,b,c},{hh,mm,ss}} = :calendar.universal_time()
    {x,y,z} = :calendar.gregorian_days_to_date(:calendar.date_to_gregorian_days({a,b,c}) + days)

    Ecto.DateTime.from_erl({{x,y,z},{hh,mm,ss}})
  end
end
