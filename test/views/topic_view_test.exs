defmodule Schoolit.TopicViewTest do
  use Schoolit.ConnCase, async: true

  test "converts markdown to html" do
    {:safe, result} = Schoolit.TopicView.markdown("**bold me**")
    assert String.contains? result, "<strong>bold me</strong>"
  end

  test "leaves text with no markdown alone" do
    {:safe, result} = Schoolit.TopicView.markdown("leave me alone")
    assert String.contains? result, "leave me alone"
  end
end
