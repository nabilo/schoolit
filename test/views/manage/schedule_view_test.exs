defmodule Schoolit.Manage.ScheduleTest do
  use Schoolit.ConnCase, async: true

  test "#grouped_by_weekday: returns schedule elements grouped by weekday" do
    schedules = [
      %{weekday: 3},
      %{weekday: 1},
      %{weekday: 2},
      %{weekday: 1},
      %{weekday: 5}
    ]

    expected_result = %{
      1 => [%{weekday: 1}, %{weekday: 1}],
      2 => [%{weekday: 2}],
      3 => [%{weekday: 3}],
      5 => [%{weekday: 5}]
    }

    assert expected_result == Schoolit.Manage.ScheduleView.grouped_by_weekday(schedules)
  end

  test "#grouped_by_weekday: returns an empty map if schedules is empty" do
    schedules = []
    expected_result = %{}

    assert expected_result == Schoolit.Manage.ScheduleView.grouped_by_weekday(schedules)
  end

  test "#grouped_by_lesson_order: returns schedule elements grouped by lesson_order" do
    schedules = [
      %{lesson_order: 3},
      %{lesson_order: 1},
      %{lesson_order: 2},
      %{lesson_order: 1},
      %{lesson_order: 5}
    ]

    expected_result = %{
      1 => [%{lesson_order: 1}, %{lesson_order: 1}],
      2 => [%{lesson_order: 2}],
      3 => [%{lesson_order: 3}],
      5 => [%{lesson_order: 5}]
    }

    assert expected_result == Schoolit.Manage.ScheduleView.grouped_by_lesson_order(schedules)
  end

  test "#cols_per_day: returns the cols count for a day dependent on the count of available days" do
    schedules = [
      %{weekday: 1},
      %{weekday: 2},
      %{weekday: 3},
      %{weekday: 4},
      %{weekday: 5},
      %{weekday: 6},
      %{weekday: 7}
    ]

    assert 1 == Schoolit.Manage.ScheduleView.cols_per_day(Schoolit.Manage.ScheduleView.grouped_by_weekday(schedules))
  end

  test "#cols_per_day: returns 1 if schedules is empty" do
    schedules = []

    assert 1 == Schoolit.Manage.ScheduleView.cols_per_day(Schoolit.Manage.ScheduleView.grouped_by_weekday(schedules))
  end

  test "#max_lessons_per_week: returns max schedule lesson_order in the whole week" do
    schedules = [
      %{lesson_order: 3},
      %{lesson_order: 1},
      %{lesson_order: 5}
    ]

    assert 5 == Schoolit.Manage.ScheduleView.max_lessons_per_week(schedules)
  end

  test "#max_lessons_per_week: returns 0 if no schedules are available" do
    schedules = []

    assert 1 == Schoolit.Manage.ScheduleView.max_lessons_per_week(schedules)
  end

  test "#remove_seconds: returns time without seconds" do
    {:ok, time} = Ecto.Time.cast("12:00:00")
    assert "12:00" == Schoolit.Manage.ScheduleView.remove_seconds(time)
  end
end
