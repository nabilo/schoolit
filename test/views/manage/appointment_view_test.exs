defmodule Schoolit.Manage.AppointmentViewTest do
  use Schoolit.ConnCase, async: true
  import Phoenix.View

  test "renders index.html", %{conn: conn} do
    appointments = [%Schoolit.Appointment{id: "1", title: "maximus@minimus.xus"},
                    %Schoolit.Appointment{id: "2", title: "minimum@xx.de"}]
    content = render_to_string(Schoolit.Manage.AppointmentView, "index.html", conn: conn, appointments: appointments)

    for appointment <- appointments do
      assert String.contains?(content, appointment.title)
    end
  end

  test "renders new.html", %{conn: conn} do
    changeset = Schoolit.Appointment.changeset(%Schoolit.Appointment{})
    content = render_to_string(Schoolit.Manage.AppointmentView, "new.html", conn: conn, changeset: changeset)

    assert String.contains?(content, "New appointment")
  end
end
