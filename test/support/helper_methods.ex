defmodule Schoolit.HelperMethods do
  def ten_oclock_future(days) do
    date_time_to_string(
      :calendar.gregorian_days_to_date(:calendar.date_to_gregorian_days(actual_date()) + days)
    )
  end

  def ten_oclock_past(days) do
    date_time_to_string(
      :calendar.gregorian_days_to_date(:calendar.date_to_gregorian_days(actual_date()) - days)
    )
  end

  defp date_time_to_string({y,m,d}), do: "#{y}-#{add_zero_to_number(m)}-#{add_zero_to_number(d)} 10:00"

  defp actual_date do
    {date, _} = :calendar.local_time
    date
  end

  defp add_zero_to_number(n) when n < 10, do: "0#{n}"
  defp add_zero_to_number(n), do: n
end
