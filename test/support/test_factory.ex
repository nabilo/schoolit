defmodule Schoolit.TestFactory do
  alias Schoolit.Repo

  require IEx
  def insert_teacher(attrs \\ %{}) do
    changes = Map.merge(%{
      title: "t",
      firstname: "Some teacher",
      lastname: "Some teacher",
      username: "teacher#{_random_text()}@xxx.de",
      password: "supersecret",
      password_confirmation: "supersecret",
      url_code: :crypto.strong_rand_bytes(5) |> Base.url_encode64 |> binary_part(0,5)
    }, attrs)

    %Schoolit.Teacher{}
    |> Schoolit.Teacher.registration_changeset(changes)
    |> Repo.insert!()
  end

  def insert_school_class(teacher, attrs \\ %{}) do
    changes = Map.merge(%{
      name: "first",
      level: 1,
      group: "Adult",
      description: "Friendly Class"
    }, attrs)

    teacher
    |> Ecto.build_assoc(:school_class, changes)
    |> Repo.insert!()
  end

  def insert_appoint(teacher, attrs \\ %{}) do

    if Map.get(attrs, :from, false) && Map.get(attrs, :to, false) do
      attrs = attrs |> Map.put(:from, "#{attrs[:from]}:00") |> Map.put(:to, "#{attrs[:to]}:00")
    end

    changes = Map.merge(%{
      description: "some content",
      title: "some content",
      from: "2010-05-19 14:00:00",
      to: "2010-05-19 14:00:00"
     }, attrs)

    {_, from_date} = Ecto.DateTime.cast(changes[:from])
    {_, to_date} = Ecto.DateTime.cast(changes[:to])
    changes = Map.merge(changes, %{from: from_date})
    changes = Map.merge(changes, %{to: to_date})
    teacher
    |> Ecto.build_assoc(:appointments, changes)
    |> Repo.insert!()
  end

  def insert_schedule(school_class, attrs \\ %{}) do
    if Map.get(attrs, :from, false) && Map.get(attrs, :to, false) do
      attrs = attrs |> Map.put(:from, "#{attrs[:from]}:00") |> Map.put(:to, "#{attrs[:to]}:00")
    end

    changes = Map.merge(%{
      title: "some content",
      from: "14:00:00",
      to: "16:00:00",
      weekday: 6
     }, attrs)

    {_, from} = Ecto.Time.cast(changes[:from])
    {_, to} = Ecto.Time.cast(changes[:to])
    changes = Map.merge(changes, %{from: from})
    changes = Map.merge(changes, %{to: to})

    school_class
    |> Ecto.build_assoc(:schedules, changes)
    |> Repo.insert!()
  end

  def insert_comment(appointment, attrs \\ %{}) do
    changes = Map.merge(%{title: _random_text(), content: _random_text()}, attrs)

    _appoint(appointment)
    |> Ecto.build_assoc(:comments, changes)
    |> Repo.insert!()
  end

  defp _appoint(appointment) do
    cond do
      is_nil(appointment) ->
        insert_appoint(insert_teacher())
      true ->
        appointment
    end
  end

  defp _random_text do
    Base.encode16(:crypto.strong_rand_bytes(5))
  end
end
