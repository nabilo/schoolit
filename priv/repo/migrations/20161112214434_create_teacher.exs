defmodule Schoolit.Repo.Migrations.CreateTeacher do
  use Ecto.Migration

  def change do
    create table(:teachers) do
      add :firstname, :string
      add :lastname, :string
      add :username, :string, null: false
      add :password_hash, :string

      timestamps
    end

    create unique_index(:teachers, [:username])
  end
end
