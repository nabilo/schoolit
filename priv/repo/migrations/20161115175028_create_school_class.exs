defmodule Schoolit.Repo.Migrations.CreateSchoolClass do
  use Ecto.Migration

  def change do
    create table(:school_classes) do
      add :level, :integer
      add :group, :string
      add :name, :string
      add :description, :string
      add :school_id, references(:schools, on_delete: :nothing)
      add :teacher_id, references(:teachers, on_delete: :nothing)

      timestamps()
    end

    create index(:school_classes, [:teacher_id])
    create index(:school_classes, [:school_id])
  end
end
