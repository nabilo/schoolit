defmodule Schoolit.Repo.Migrations.AddUrlCodeToTeachers do
  use Ecto.Migration

  def change do
    alter table(:teachers) do
      add :url_code, :string
    end

    create index(:teachers, [:url_code])
  end
end
