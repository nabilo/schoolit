defmodule Schoolit.Repo.Migrations.AddTitleToTeachers do
  use Ecto.Migration

  def change do
    alter table(:teachers) do
      add :title, :string
    end
  end
end
