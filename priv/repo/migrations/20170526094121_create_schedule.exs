defmodule Schoolit.Repo.Migrations.CreateSchedule do
  use Ecto.Migration

  def change do
    create table(:schedules) do
      add :title, :string
      add :school_class_id, :integer
      add :teacher_id, :integer
      add :content, :text
      add :weekday, :integer
      add :from, :time
      add :to, :time
      add :lesson_order, :integer

      timestamps()
    end

  end
end
