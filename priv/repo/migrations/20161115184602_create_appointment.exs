defmodule Schoolit.Repo.Migrations.CreateAppointment do
  use Ecto.Migration

  def change do
    create table(:appointments) do
      add :title, :string
      add :description, :text
      add :from, :datetime
      add :to, :datetime
      add :teacher_id, references(:teachers, on_delete: :nothing)
      add :school_class_id, references(:school_classes, on_delete: :nothing)

      timestamps()
    end

    create index(:appointments, [:teacher_id])
  end
end
