defmodule Schoolit.Repo.Migrations.CreateSchool do
  use Ecto.Migration

  def change do
    create table(:schools) do
      add :name, :string
      add :street, :string
      add :zip_code, :string
      add :city, :string
      add :subdomain, :string

      timestamps()
    end

    create unique_index(:schools, [:subdomain])
  end
end
