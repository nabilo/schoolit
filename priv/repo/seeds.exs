# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Schoolit.Repo.insert!(%Schoolit.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
#
alias Schoolit.Repo
alias Schoolit.SchoolClass
alias Schoolit.Teacher
alias Schoolit.School

school = Repo.get_by(School, subdomain: "koppenplatz")
unless school do
  school = Repo.insert!(%School{name: "Grundschule am Koppenplatz", subdomain: "koppenplatz"})
end

teacher = Repo.get_by(Teacher, lastname: "Renneisen", username: "lehrerin")
unless teacher do
  teacher = Repo.insert!(
             %Teacher{
               title: "x",
               lastname: "Renneisen",
               username: "lehrerin",
               password_hash: Comeonin.Bcrypt.hashpwsalt("password"),
               url_code: "aaaaa"}
)
end

school_class = Repo.get_by(SchoolClass, level: 3, group: "C", school_id: school.id)
unless school_class do
  school_class = Repo.insert!(%SchoolClass{level: 3, group: "C", school_id: school.id, teacher_id: teacher.id})
end
