defmodule Schoolit.Comment do
  use Schoolit.Web, :model

  @required_fields [:title, :content, :appointment_id]

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> assoc_constraint(:appointment)
  end

  schema "comments" do
    field :title, :string
    field :content, :string
    field :user_id, :integer
    belongs_to :appointment, Schoolit.Appointment

    timestamps()
  end
end
