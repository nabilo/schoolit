defmodule Schoolit.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :title, :string
      add :content, :text
      add :user_id, :integer
      add :appointment_id, references(:appointments, on_delete: :delete_all)

      timestamps
    end

    create index(:comments, [:appointment_id])
  end
end
